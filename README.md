# small-orm-swoole
Small Orm is a small php ORM.

This package provide swoole connector.

## Install

Require Small ORM Core package (https://framagit.org/small/small-orm-core) :
```
composer require small/orm-core
```

Require the package with composer:
```
composer require small/orm-swoole
```

## Documentation

You can find the documentation here : [documentation](https://framagit.org/small/small-orm-doc)
